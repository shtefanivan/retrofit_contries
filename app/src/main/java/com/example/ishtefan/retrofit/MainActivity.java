package com.example.ishtefan.retrofit;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {


//    public String name;

    public ArrayList<String> names;
    RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.action);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit.getCountry("Moscow", new Callback<List<Country>>() {
                    @Override
                    public void success(List<Country> countries, Response response) {
                        Toast.makeText(MainActivity.this, countries.get(0).name, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        });


        //init in Activity
        list = (RecyclerView) findViewById(R.id.recycleview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        list.setLayoutManager(layoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Retrofit.getContries(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {
                list.setAdapter(new RecyclerViewAdapter(countries, MainActivity.this));
                Toast.makeText(MainActivity.this, countries.get(1).name, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "ошибка", Toast.LENGTH_SHORT).show();
            }
        });
    }


}

