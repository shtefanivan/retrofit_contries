package com.example.ishtefan.retrofit;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ishtefan on 08.10.2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Country> strings;
    private Context context;

    public RecyclerViewAdapter(List<Country> strings, Context context) {
        this.strings = strings;
        this.context = context;
    }

    //    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        View v = getLayoutInflater().inflate(R.layout.one_element_view, viewGroup, false);
//        return new RecyclerView.ViewHolder(v);
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(strings.get(position).name);
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
//        viewHolder.name.setText(strings.get(i));
//    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(View item) {
            super(item);
            name = (TextView) item.findViewById(R.id.textView);
        }
    }

}

