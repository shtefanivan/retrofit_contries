package com.example.ishtefan.retrofit;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by ishtefan on 18.10.2016.
 */

public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
private static ApiReference apiReference;

    static {
        initialize();
    }

    static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiReference = restAdapter.create(ApiReference.class);
    }

    interface ApiReference{
        @GET("/v1/all")
        void getContries(Callback<List<Country>> callback);

        @GET("/v1/capital/{capital}")
        void getCountry(@Path("capital") String capital, Callback<List<Country>> callback);

    }

    public static void getContries(Callback<List<Country>> callback) {
        apiReference.getContries(callback);
    }

    public static void getCountry(String capital, Callback<List<Country>> callback) {
        apiReference.getCountry(capital, callback);
    }
}
